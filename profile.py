"""Start X11VNC on a node. 

Instructions:
Wait for the experiment to start, click on the VNC option in the node context menu.
"""

# Import the Portal object.
import geni.portal as portal
# Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec() 

#
# Declare that you will be starting X11 VNC on (some of) your nodes.
# You must have this line for X11 VNC to work.
#
request.initVNC()

# Optional physical type for all nodes.
pc.defineParameter("phystype",  "Optional physical node type",
                   portal.ParameterType.STRING, "",
                   longDescription="Specify a physical node type (pc3000,d710,etc) " +
                   "instead of letting the resource mapper choose for you.")

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()
 
# Add a raw PC to the request.
node = request.RawPC("node")
if params.phystype != "":
    node.hardware_type = params.phystype
    pass

#
# Install and start X11 VNC. Calling this informs the Portal that you want a VNC
# option in the node context menu to create a browser VNC client.
#
# If you prefer to start the VNC server yourself (on port 5910) then add nostart=True. 
#
node.startVNC()

portal.context.printRequestRSpec()
